function setAllTeacherSelectors(){
	$(".teachSelect").val(function(){return $(this).attr("data-selID");});
}

function updateTeacherPart(partID,theSelect)
{
	document.getElementById("AJAXresponse").innerHTML = 'updating teacher';
	teacherID = theSelect.options[theSelect.selectedIndex].value;
	updateTable("Course part","TeacherID",partID,theSelect.options[theSelect.selectedIndex]);
//		theURL = "courseParts.php?partID="+partID+"&teacherID="+teacherID;
//		location.href=theURL;
}
function updateTeacherType(teacherID)
{
    theSelect = document.getElementById("selectTypeTeacher"+teacherID);
    document.getElementById("AJAXresponse").innerHTML = 'updating teacher type';
    updateTable("teachers","typeID",teacherID,theSelect.options[theSelect.selectedIndex]);
}


function updateTable(TableName,ColumnName,id,obj){
	$.post("teachCourseFunctions.php",
		{
			actionType:"updateTable",
			TableName:TableName,
			ColumnName:ColumnName,
			ID:id,
			value:obj.value
		},
		function (data,status){
			if (status == "success"){
				$("#AJAXresponse").html(data);
			}else{
				alert("Er is een fout opgetreden. " + data + " " + status);
				return false;
			}
		});
}

function updateNrStudents(courseID,ayID,nrStudents){
	$.post("teachCourseFunctions.php",
		{
			actionType:"updateNrStudents",
			courseID:courseID,
			AYID:ayID,
			nstudents:nrStudents
		},
		function (data,status){
			if (status == "success"){
				$("#AJAXresponse").html(data);
			}else{
				alert("Er is een fout opgetreden. " + data + " " + status);
				return false;
			}
		});
}

function updateResponsiblePart(partID,obj){
	if (obj.checked){obj.value = 1;}else{obj.value = 0;}
	updateTable('Course part','responsible',partID,obj);
}
function updateActiveTeacher(partID,obj){
	if (obj.checked){obj.value = 1;}else{obj.value = 0;}
	updateTable('teachers','active',partID,obj);
}
