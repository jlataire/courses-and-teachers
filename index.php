<!DOCTYPE html>
<html>
<head>
<head><title>Courses and Teachers</title>
<script src="teachCourseScripts.js"></script>

<script>
function confirmRemovePart(courseID,partID){
	if (confirm("Are you sure that you want to remove this part ?")){
		location.href = "index.php?editCourseID="+courseID+"&removePartID="+partID;
	}
// 	
}
</script>

<link rel="stylesheet" type="text/css" href="teachCourseStyles.css">

<!-- <script src="jquery-3.3.1.min.js"></script> -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script>
$(document).ready(function(){
	setAllTeacherSelectors();
});
</script>
</head>
<body>

<p id="AJAXresponse">The AJAX response</p>

<?php 


	
	require 'teachCourseFunctions.php';
	
	echo '<h1>Academic Year '.$academicYear.'</h1>';
	echo '<h2>Courses and Teachers</h2>';
	echo navigationBar();
	

	if (!empty($_GET['removePartID']) and (is_numeric($_GET['removePartID']))){
		$remPartID = $_GET['removePartID'];
		$theRemQuery = 'DELETE FROM `course part` WHERE id='.$remPartID;
		writeToLog($theRemQuery);
		$res = $conn->query($theRemQuery);
		echo '<script>document.getElementById("AJAXresponse").innerHTML = "'.$theRemQuery.', '.$res.'";
			</script>';
	}
		
	if (!empty($_GET['editCourseID']) and (is_numeric($_GET['editCourseID'])))
	{
		$editID = $_GET['editCourseID'];
		$editOneCoursePart = true;
	}else
	{
		$editOneCoursePart = false;
	}
	if (!empty($_GET['editTeachers']) and ($_GET['editTeachers'])){
		$editTeachers = true;
	}
	else{
		$editTeachers = false;
	}
	
//	echo 'Connected';
	$theQuery = 'SELECT Teachers.active, Courses.NameCourse,Courses.id AS CourseID,`Course part`.TeacherID, Courses.Comments, Teachers.lastname,Teachers.firstname,`Course part`.typePart,`Course part`.NamePart,`Course part`.id AS partID, Titularis.lastname AS lastnameTitular, Titularis.firstname AS firstnameTitular, TitularID, `Course part`.loadpart, `Course part`.responsible, Courses.caliID, sem1, sem2  
	FROM `Course part` 
	RIGHT JOIN Teachers ON `Course part`.TeacherID=Teachers.id RIGHT OUTER JOIN Courses ON `Course part`.CourseID=Courses.id INNER JOIN Teachers AS Titularis ON Courses.TitularID=Titularis.id';
	if ($editOneCoursePart){
		$theQuery .= " WHERE Courses.id = $editID";
	}
	else
	{

		$theQuery .=  ' WHERE 1';
	}
	$theQuery .= ' ORDER BY Courses.NameCourse,`Course part`.typePart,`Course part`.responsible DESC,Teachers.lastname';
	
// 	echo $theQuery.'<br />';
	$allCoursePartsSQL = $conn->query($theQuery);
	$currentCourseID = 0;
	$teacherOptionList = teacherOptionList($conn);

	while ($oneCoursepart=mysqli_fetch_array($allCoursePartsSQL)){

		if ($currentCourseID != $oneCoursepart['CourseID']){
			if ($currentCourseID != 0){
				echo '</table></div>';
				}
			echo '<div id="headCourse'.$oneCoursepart['CourseID'].'" class="courseDiv">';

			if ($editOneCoursePart){

				echo '<h3><input class="headinginput" type="text" value="'.$oneCoursepart['NameCourse'].'" onblur="updateTable('."'Courses','NameCourse',".$oneCoursepart['CourseID'].",this)".'" size=47 maxlength=100 ></h3>';
				echo '<p class="comment"><input type="text" value="'.$oneCoursepart['caliID'].'" onblur="updateTable('."'Courses','caliID',".$oneCoursepart['CourseID'].",this)".'" size=15 maxlength=15 placeholder="Cali ID" ></p>';
                echo '<p class="comment"><input type="text" value="'.$oneCoursepart['Comments'].'" onblur="updateTable('."'Courses','Comments',".$oneCoursepart['CourseID'].",this)".'" size=47 maxlength=150 placeholder="Comments" ></p>';
				echo '<p>Titularis: <select class="teachSelect" data-selID="'.$oneCoursepart['TitularID'].'"  onchange="updateTable('."'Courses','TitularID',".$oneCoursepart['CourseID'].",this)".'">';
				echo $teacherOptionList;
				echo '</select></p>';
				echo '<br />';
			}
			else{
                
				echo '<header><h3>'.$oneCoursepart['NameCourse'];
				echo ' <span class="coursetitular">- '.$oneCoursepart['firstnameTitular'].' '.$oneCoursepart['lastnameTitular'].'</span>';
// 				echo $oneCoursepart['firstnameTitular'].' '.$oneCoursepart['lastnameTitular'].')';
				echo '</h3></header>';
				echo '<p class="editlink">';
                if (!empty($oneCoursepart['caliID'])){
                    echo '<a href="https://caliweb.cumulus.vub.ac.be/caliweb/?page=course-offer&id='.$oneCoursepart['caliID'].'&year='.$academicYearShort.'&language=en">Cali</a> - ';
                }
                echo "<a href=index.php?editCourseID=".$oneCoursepart['CourseID'].">&#9998;</a></p>";
				echo '<p class="comment">';
				if ($oneCoursepart['sem1']){echo '&#10112; ';}
				if ($oneCoursepart['sem2']){echo '&#10113; ';}
				if (!empty($oneCoursepart['Comments'])){
					echo $oneCoursepart['Comments'];
				}
				echo '</p>';
				echo '<table>';
			}
		}
		$currentCourseID = $oneCoursepart['CourseID'];
		if (!empty($oneCoursepart['partID'])){
			


			if (($editTeachers) or ($editOneCoursePart)){
				if ($oneCoursepart['responsible']){
					$responsible = 'checked ';}else{$responsible = '';}
				echo '<input type="checkbox" '.$responsible.'onchange="updateResponsiblePart('.$oneCoursepart['partID'].',this)">';
				echo '<select  class="teachSelect" data-selID="'.$oneCoursepart['TeacherID'].'"  onchange="updateTeacherPart('.$oneCoursepart['partID'].',this)">';
				echo $teacherOptionList;
				echo '</select>';
			}
			else{
				if ((!$oneCoursepart['active']) or ($oneCoursepart['TeacherID'] == 1)){ // either not active, or not assigned (Teacher ID = 1)
					echo '<tr class="notActive">';
				}
				elseif ($oneCoursepart['responsible']){
					echo '<tr class="responsible">';
				}
				else{
					echo '<tr>';
				}
				echo '<td style="width:30ex"><a href="teacherParts.php#headTeacher'.$oneCoursepart['TeacherID'].'" target="windowTeachers">'.$oneCoursepart['firstname'].' '.$oneCoursepart['lastname'].'</a>';
			
//				echo $oneCoursepart['firstname'].' '.$oneCoursepart['lastname'];
				echo '</td>';
			}

			if ($editOneCoursePart){
				echo '<input type="text" size="10" value="'.$oneCoursepart['typePart'].'" onblur="updateTable('."'Course part','typePart',".$oneCoursepart['partID'].",this)".'" >';
				echo '<input type="text" size="35" value="'.$oneCoursepart['NamePart'].'" onblur="updateTable('."'Course part','NamePart',".$oneCoursepart['partID'].",this)".'" placeholder="Description, comment, ..." >';
				echo '<input type="number" min="0" max="9000" value="'.$oneCoursepart['loadpart'].'" onblur="updateTable('."'Course part','loadpart',".$oneCoursepart['partID'].",this)".'" >';
				echo '<a href="javascript:confirmRemovePart('.$oneCoursepart['CourseID'].','.$oneCoursepart['partID'].')">remove</a><br />';
			} else {
				echo '<td style="width:4ex">'.$oneCoursepart['typePart'].'</td><td style="width:40ex">'.$oneCoursepart['NamePart'].'</td><td style="width:5ex">'.$oneCoursepart['loadpart'].'</td>';
			}
			echo '</tr>';
		}

	}

	if (!$editOneCoursePart){
		echo '</table>';		
	}
	else{
		echo '<h4>Add new part</h4>';
		echo '<form method="post" action="index.php?editCourseID='.$editID.'">
				<input type="hidden" name="actionType" value="addCoursePart" />
				<input type="hidden" name="courseID" value="'.$editID.'" />
					<select name="teacherID">';
		echo $teacherOptionList;
		echo '</select>
					<input name="typePart" type="text" placeholder="Type (HOC / WPO)" />
					<input name="partName" type="text" placeholder="Description, comment, ..." />
					<input type="submit" value="add" />
				</form> ';
		echo '<p class="editlink">'."<a href=index.php#headCourse".$editID.">done</a></p>";
	
	}
	echo '</div>';
	echo '<p class="SQLquery">'.$theQuery.'</p>';
	mysqli_close($conn);
?>

</body>
</html>
