<?php
	date_default_timezone_set("Europe/Brussels");
	
	require 'openMySQLconn.php';
	if ($conn -> connect_error){die("Could not connect");}

	
	$academicYearNum = 2019;
	$academicYear = $academicYearNum.' - '.($academicYearNum + 1);
    $academicYearStart = sprintf("%u-09-01",$academicYearNum);
    $academicYearShort = sprintf("%u%u",$academicYearNum - 2000,$academicYearNum+1 - 2000);
	
	$actionType = $_POST['actionType'];
	if (!empty($actionType))
	{
		switch ($actionType){
			case "addTeacher":
				addTeacher($conn,secureString($conn,$_POST['firstname']),secureString($conn,$_POST['lastname']));
				break;
			case "addCourse":
				addCourse($conn,$_POST['name'],$_POST['titularID']);
				break;
			case "addCoursePart":
				echo 'Adding course part ';
				echo $_POST['courseID'].' '.$_POST['teacherID'].' '.$_POST['typePart'].' '.secureString($conn,$_POST['partName']);
				if (addCoursePart($conn,$_POST['courseID'],$_POST['teacherID'],$_POST['typePart'],$_POST['partName']))
				{echo 'OK';}else{echo 'Failed';}
				break;
			case "updateTable":
				//echo 'Updating the table';
				//echo $_POST['TableName'].$_POST['ColumnName'].$_POST['ID'].$_POST['value'];
				updateTable($conn,$_POST['TableName'],$_POST['ColumnName'],$_POST['ID'],secureString($conn,$_POST['value']));
				break;		
			case "updateNrStudents":
				updateNrStudents($conn,secureString($conn,$_POST['courseID']),secureString($conn,$_POST['AYID']),secureString($conn,$_POST['nstudents']));
				break;
			default:
				echo 'unknown actionType';
		}
	}
	
	function updateTable($conn,$tableName,$columnName,$id,$value)
	{
        if ($value == ''){$value = 'NULL';}
        else {$value = "'$value'";}
		$theQuery = "UPDATE `$tableName` SET `$columnName`=$value WHERE id=$id";
		echo $theQuery;
		switch ($tableName){
			case "Course part":
				switch ($columnName){
					case "TeacherID":
						$infQuery = "SELECT firstname,lastname,NameCourse,typePart,NamePart FROM `course part` RIGHT JOIN teachers ON `course part`.TeacherID=teachers.id RIGHT JOIN courses ON `course part`.CourseID=courses.id WHERE `course part`.id=$id";
						$infres = $conn->query($infQuery);
						$infContent = $infres->fetch_assoc();
						
						$infQuery ="SELECT firstname,lastname FROM teachers WHERE id=$value";
						$infres2 = $conn->query($infQuery);
						$newTeacher = $infres2->fetch_assoc();
						
						$infChange = $infContent['NameCourse'].' '.$infContent['typePart'].' '.$infContent['NamePart'].': '.$newTeacher['firstname'].' '.$newTeacher['lastname'].' (was '.$infContent['firstname'].' '.$infContent['lastname'].')';
						break;
				}
				break;
		}
		$result = $conn->query($theQuery);
		if ($result)
		{
			echo ' OK'; 
			updateLastChangeTimeStamp($conn);
			if ($infChange){
				$theQuery .= ' '.$infChange;
			}
			writeToLog($theQuery);
		}
		else {echo ' FAILED';}
	}
	
	function updateNrStudents($conn,$courseID,$ayid,$nstud)
	{
// 		echo "Course $courseID, ay $ayid, nstud $nstud";

		$res = $conn->query("SELECT * FROM linkCourseAY WHERE `courseID`=$courseID AND `AYID`=$ayid");
		if ($nstud == ''){
			$theQuery = "DELETE FROM linkCourseAY WHERE `courseID`=$courseID AND `AYID`=$ayid";
		}
		elseif (($res->num_rows) > 0){
			$theQuery = "UPDATE linkCourseAY SET nrStudents=$nstud WHERE `courseID`=$courseID AND `AYID`=$ayid";
		}else{
			$theQuery = "INSERT INTO linkCourseAY (`nrStudents`,`courseID`,`AYID`) VALUES($nstud,$courseID,$ayid)";
		}
		$res = $conn->query($theQuery);
		echo $theQuery;
		if ($res)
		{
			echo ' OK'; 
			updateLastChangeTimeStamp($conn);
			writeToLog($theQuery);
		}
		else {echo ' FAILED';}
	}
	function updateLastChangeTimeStamp($conn)
	{
		$date=date_create();
		$theQuery = "UPDATE info SET lastchange=".date_timestamp_get($date)." WHERE 1";
		$result = $conn->query($theQuery);
// 		echo $theQuery;
		if ($result){
			//echo 'Last change updated';
			}
		else{echo 'Last change update FAILED';}
	}
	function getLastChangeTime($conn,$compact = 0)
	{
		$result = $conn->query('SELECT lastchange FROM info WHERE 1');
		$theDate = mysqli_fetch_assoc($result);
        if ($compact){
            return date('Ymd_His',$theDate['lastchange']);
        }
        else{return date('d/m/Y - G:i:s',$theDate['lastchange']);}
	}
	
	function addTeacher($conn,$newFirstname,$newLastname)
	{
		if (!empty($newFirstname) and !empty($newLastname))
		{
			$theQuery = "INSERT INTO `Teachers` (`firstname`, `lastname`,`active`) VALUES ('$newFirstname','$newLastname',1)";
			writeToLog($theQuery);
			return $conn->query($theQuery);
		}
	}
	function addCourse($conn,$coursename,$titularID){
		if (!empty($coursename) and !empty($titularID))
		{
			$theQuery = "INSERT INTO `Courses` (`NameCourse`, `TitularID`) VALUES ('$coursename','$titularID')";
			writeToLog($theQuery);
			return $conn->query($theQuery);
		}
	}
	function addCoursePart($conn,$courseID,$teacherID,$typePart,$partName){
		if (!empty($courseID) and !empty($teacherID) and !empty($typePart))
		{
			$theQuery = "INSERT INTO `Course part` (`CourseID`, `TeacherID`,`typePart`,`NamePart`) VALUES ('$courseID','$teacherID','$typePart','$partName')";
			echo "<br />$theQuery <br />";
			$result = $conn->query($theQuery);
			$newID = $conn->insert_id;
			
			$query2 = "SELECT * FROM `Course part` RIGHT JOIN courses ON `course part`.CourseID=courses.id RIGHT JOIN teachers ON `course part`.TeacherID=teachers.id WHERE `course part`.id=".$newID;
			echo "<br />".$query2."<br />";
			$qCourse = $conn->query($query2);
			$theCourse = $qCourse->fetch_assoc();
			
			writeToLog($theQuery.' '.$theCourse['NameCourse'].', '.$theCourse['firstname'].' '.$theCourse['lastname']);

			return $result;
		}
	}	
	
	function fetchTableContentJSON($conn,$colnames,$tableName,$where,$orderby)
	{	

		$tableName = $conn->real_escape_string($tableName);

		if (isset($colnames) and !empty($colnames))
		{$colnames = $conn->real_escape_string($colnames);
		}else {$colnames = '*';}


		$selectQuery = "SELECT $colnames FROM $tableName";
		if (isset($where) and !empty($where)){
			$selectQuery .= " WHERE ".$conn->real_escape_string($where);
		}
		if (isset($orderby) and !empty($orderby)){
			$selectQuery .= " ORDER BY ".$conn->real_escape_string($orderby);
		}

		$result = $conn->query($selectQuery);

		if ($conn->errno){
			die($conn->error.' query: '.$selectQuery);
		}

		$outp = "";
		while($rs = $result->fetch_array(MYSQLI_ASSOC)) {
			$outp .= '{';
			foreach ($rs as $fieldname => $fieldcontent){
				$outp .= '"'.$fieldname.'": "'.$fieldcontent.'",';
			}
			$outp = rtrim($outp,',');
			$outp .= '},';
		}

		$outp = rtrim($outp,',');
		return '{"records":['.$outp.']}';

	}
	
	function writeToLog($theText)
	{
		$myfile = fopen("myLog.txt","a") or die("Unable to open file");
// 		echo "File opened";
		
		fwrite($myfile,date('d/m/Y - G:i:s',date_timestamp_get(date_create())).": ".$theText."\r");

		fclose($myfile);
	}
	
	function getTeacherByID($conn,$theID){
		$theQuery=$conn->query('SELECT * FROM Teachers WHERE id='.$theID);
	}
	
	function getTeachersSQL($conn){
		return $conn->query('SELECT * FROM `Teachers` WHERE active=1 ORDER BY lastname');
	}
	function getTeachersLoadSQL($conn,$onlyActive,$sortOrder){
		if ($onlyActive == 1) {$condition =' WHERE Teachers.active=1';}
		elseif ($onlyActive == 2){$condition = ' WHERE Teachers.active=1 OR loadpart > 0';}
		else {
		$condition = '';
		}
		switch ($sortOrder){
			case 1:
				$theOrder = 'totalLoad DESC';
				break;
			case 2:
				$theOrder = 'endDate';
				break;
            case 3:
                $theOrder = 'startDate';
                break;
            case 4:
                $theOrder = 'TypeTeacher.ID,Teachers.lastname';
                break;
			default:
				$theOrder = 'Teachers.lastname';				
		}
		$theQuery = 'SELECT Teachers.startDate,Teachers.endDate,Teachers.active,Teachers.firstname, Teachers.lastname, Teachers.id, Teachers.comments, sum(loadpart) AS totalLoad, TypeTeacher.Name AS teacherType, TypeTeacher.ID AS typeID FROM `Course part` RIGHT JOIN Teachers ON Teachers.id=TeacherID LEFT JOIN TypeTeacher ON Teachers.typeID=TypeTeacher.ID '.$condition.' GROUP BY Teachers.id ORDER BY '.$theOrder;
		return $conn->query($theQuery);
	}
	
	function getCoursesSQL($conn){
		return $conn->query('SELECT * FROM `Courses` WHERE 1 ORDER BY NameCourse');
	}
	
	function teacherOptionList($conn){
		$allTeachers = getTeachersSQL($conn);
		$val = '';
		while ($oneTeacher=mysqli_fetch_array($allTeachers)){
			$val .= '<option id='.$oneTeacher['id'].' value='.$oneTeacher['id'].'>'.$oneTeacher['firstname'].' '.$oneTeacher['lastname'].'</option>';
		}
//         echo $val;
		return $val;
	}
    function teacherTypeList($conn){
        $allTypes = $conn->query("SELECT * FROM TypeTeacher");
        $val = '';
        while ($oneType=mysqli_fetch_array($allTypes)){
            $val .= '<option id='.$oneType['ID'].' value='.$oneType['ID'].'>'.$oneType['Name'].'</option>';
        }
        return $val;
    }
	
	function CourseOptionList(){
		$allTeachers = getTeachersSQL($conn);
		$val = '';
		while ($oneTeacher=mysqli_fetch_array($allTeachers)){
			$val .= '<option value='.$oneTeacher['id'].'>'.$oneTeacher['NameCourse'].'</option>';
		}
		return $val;
	}
	
	function navigationBar(){
		return '<p><a href="courses.php">Courses</a> - 
		<a href="teachers.php?onlyactive=true">Teachers</a> (<a href="teachers.php?onlyactive=false">all</a>) - 
		<a href="index.php">Courses and Teachers</a> (<a href="index.php?editTeachers=true">edit</a>) - 
		<a href="teacherParts.php">Teachers and Courses</a> - 
		<a href="CoursesTeachersHTML?showLoad=1">Export HTML</a> 
        (<a href="CoursesTeachersHTML?showLoad=false">no load</a> - order by: <a href="CoursesTeachersHTML?orderby=load">load</a> - <a href="CoursesTeachersHTML?orderby=enddate">date</a> - <a href="CoursesTeachersHTML?orderby=startdate">start phd</a>)</p>';
	}
    
    function timeUntilStartAcademicYear($oneStartDate){// used to determine the time someone has already been working on PhD
        if (empty($oneStartDate)){
            return '';
        }

        global $academicYearStart;

        $dateAcYear = date_create($academicYearStart);
        $dateStart = date_create($oneStartDate);
        $dateLength = $dateStart->diff($dateAcYear);
        return $dateLength->format("%r%yy %mm %dd");
    }
	function endDateMode($oneEndDate){
		$oneDateP = date_create($oneEndDate);
		global $academicYearNum;
		
		$dateStr = sprintf("%u-11-01",$academicYearNum);
// 		$dateStr = ($academicYearNum).'-11-01';
		if (date_interval_format(date_diff($oneDateP,date_create($dateStr)),"%r%a") > 0){
			// not available in first semester
			return 3;
		}
		$dateStr = sprintf("%u-02-01",$academicYearNum + 1);
		if (date_interval_format(date_diff($oneDateP,date_create($dateStr)),"%r%a") > 0){
		    // not available in second semester
			return 2;
		}
		$dateStr = sprintf("%u-06-31",$academicYearNum + 1);
		if (date_interval_format(date_diff($oneDateP,date_create($dateStr)),"%r%a") > 0){
			// not available for second session exams		
			return 1;
		}
		// available during whole academic year
		return 0;

	}
	function secureString($conn,$theString)
    // watch out: this function requires a working mySQL connection
    {
        return mysqli_real_escape_string($conn,htmlentities($theString,ENT_QUOTES,'utf-8'));
    }
    
		
	?>
