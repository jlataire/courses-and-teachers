<?php
// header("Access-Control-Allow-Origin: *");
header("Content-Type: text/tsv; charset=UTF-8");
header('Content-Disposition: attachment; filename="TableContents.tsv"');

	require 'openMySQLconn.php';
	if ($conn -> connect_error){die("Could not connect");}
	require 'teachCourseFunctions.php';

$sep = "\t";

if (isset($_POST['tableSpec']) and !empty($_POST['tableSpec'])){
	$tableSpec = $_POST['tableSpec'];
	$tableSpec = $conn->real_escape_string($tableSpec);
	
	$ayquery = ("SELECT * FROM AcademicYear ORDER BY startDate");
	$allAY = $conn->query($ayquery);
	
	if ($conn->errno){
		echo ('Error: '.$conn->error);
	}

	
	$selectQuery = "SELECT $tableSpec";
	
	$result = $conn->query($selectQuery);
	
	if ($conn->errno){
		die($conn->error.' query: '.$selectQuery);
	}

	$outp = "";

	$firstRow = true;
	while($rs = $result->fetch_array(MYSQLI_ASSOC)) {
		if ($firstRow){
			foreach ($rs as $fieldname => $fieldcontent){
				$outp .= '"'.$fieldname.'"'.$sep;
			}

			$allAY->data_seek(0);
			while ($ay1 = $allAY->fetch_array(MYSQLI_ASSOC)){
				$d = new DateTime($ay1['startDate']);
				$outp .= '"'.$d->format('Y').'"'.$sep;		
			}
			$firstRow = false;
			$outp = rtrim($outp,$sep);
			$outp .= "\r\n";
		}
		$allAY->data_seek(0);
			
		foreach ($rs as $fieldname => $fieldcontent){
			if ($fieldname == "NameCourse"){
				$outp .= '=HYPERLINK("https://caliweb.cumulus.vub.ac.be/caliweb/?page=course-offer&id='.$rs['caliID'].'&anchor=1&target=pr&year='.$academicYearShort.'&language=en&output=html","'.$fieldcontent.'")'.$sep;
// 				$outp .= '"'.$fieldcontent.'"'.$sep;
			}else{
				$outp .= '"'.$fieldcontent.'"'.$sep;
				}
		}
		$numstudq = "SELECT * FROM `AcademicYear` LEFT JOIN `linkCourseAY` ON `linkCourseAY`.`AYID`=`AcademicYear`.`id` WHERE `linkCourseAY`.`courseID`=".$rs['cid']." ORDER BY startDate";
		// only the rows for filled in nrstudents.
		$resst = $conn->query($numstudq);
		$oneResst = $resst -> fetch_array(MYSQLI_ASSOC);
		
		while ($ay1 = $allAY->fetch_array(MYSQLI_ASSOC)){
			if ($oneResst['AYID'] == $ay1['id'])
			{
				$outp .= '"'.$oneResst['nrStudents'].'"'.$sep;
				$oneResst = $resst -> fetch_array(MYSQLI_ASSOC);
			}
			else{$outp.= '""'.$sep;}
		}
		$outp = rtrim($outp,$sep);
		$outp .= "\r\n";
	}
} else
{
	$outp = "No table name provided";
}

$conn->close();

echo($outp);
?>