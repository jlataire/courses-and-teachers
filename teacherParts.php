<!DOCTYPE html>
<html>
<head>
<title>Teachers and Courses</title>
<script src="teachCourseScripts.js"></script>
<link rel="stylesheet" type="text/css" href="teachCourseStyles.css">
<!-- <script src="jquery-3.3.1.min.js"></script> -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script>
$(document).ready(function(){
	setAllTeacherSelectors();
});
</script>

</head>
<body>

<p id="AJAXresponse">The AJAX response</p>

<?php 


	
	require 'teachCourseFunctions.php';
	echo '<h1>Academic Year '.$academicYear.'</h1>';
	
	echo '<h2>Teachers and Courses</h2>';
	
	echo navigationBar();

	
	if (!empty($_GET['editTeacherID']) and (is_numeric($_GET['editTeacherID'])))
	{
		$editID = $_GET['editTeacherID'];
		$editOneTeacher = true;
	}else
	{
		$editOneTeacher = false;
	}

	
//	echo 'Connected';
	$theQuery = 'SELECT Courses.NameCourse,Courses.id AS CourseID,`Course part`.TeacherID, Teachers.active, Teachers.comments, Teachers.lastname, Teachers.firstname, `Course part`.typePart,`Course part`.NamePart,`Course part`.id AS partID, Titularis.lastname AS lastnameTitular, Titularis.firstname AS firstnameTitular, TitularID, `Course part`.loadpart, sem1, sem2
	 FROM `Course part` RIGHT JOIN Teachers ON `Course part`.TeacherID=Teachers.id RIGHT OUTER JOIN Courses ON `Course part`.CourseID=Courses.id INNER JOIN Teachers AS Titularis ON Courses.TitularID=Titularis.id'; 
	if ($editOneTeacher){
		$theQuery .= " WHERE TeacherID = $editID";
	}
	else
	{

		$theQuery .=  ' WHERE 1';
	}
	$theQuery .= ' ORDER BY Teachers.lastname, Teachers.firstname, Courses.NameCourse,`Course part`.typePart';
	

	$allCoursePartsSQL = $conn->query($theQuery);
	$currentTeacherID = 0;
	$teacherOptionList = teacherOptionList($conn);

	while ($oneTeacherPart=mysqli_fetch_array($allCoursePartsSQL)){

		if ($currentTeacherID != $oneTeacherPart['TeacherID']){
			if ($currentTeacherID != 0){
				echo $totLoadString;
				echo '</table></div>';
				}
			echo '<div id="headTeacher'.$oneTeacherPart['TeacherID'].'" class="courseDiv">';
			$theLoad = 0;

			
			echo '<header><h3';
			if (!$oneTeacherPart['active']){echo ' class="notActiveTeacher"';}
			echo '>'.$oneTeacherPart['firstname'].' '.$oneTeacherPart['lastname'].'</h3></header>';
			if (!$editOneTeacher){
				echo '<p class="editlink">'."<a href=?editTeacherID=".$oneTeacherPart['TeacherID'].">&#9998;</a></p>";
			}
			if (!empty($oneTeacherPart['comments'])){
				echo '<p class="comment">'.$oneTeacherPart['comments'].'</p>';
			}
			
			echo '<table>';

		}
		$currentTeacherID = $oneTeacherPart['TeacherID'];
		if (!empty($oneTeacherPart['partID'])){
			echo '<tr>';
			if ($editOneTeacher){
				echo '<td><select class="teachSelect" data-selID="'.$currentTeacherID.'"  onchange="updateTeacherPart('.$oneTeacherPart['partID'].',this)">';
				echo $teacherOptionList;
				echo '</select></td>';
			}
			echo '<td style="width:40ex">';
			
			echo '<a href="index.php#headCourse'.$oneTeacherPart['CourseID'].'" target="windowCourses">'.$oneTeacherPart['NameCourse'].'</a> ';
			echo '</td>';
			
			echo '<td style="width:4ex">';
			if ($oneTeacherPart['sem1']){echo '&#10112; ';}else{echo '&nbsp;&nbsp;&nbsp;';}
			if ($oneTeacherPart['sem2']){echo '&#10113; ';}
			echo '</td>';
			echo '<td style="width:6ex">';
			echo $oneTeacherPart['typePart'].'</td>';
			if ($editOneTeacher){
				echo '<td><input type="text" size="35" value="'.$oneTeacherPart['NamePart'].'" onchange="updateTable('."'Course part','NamePart',".$oneTeacherPart['partID'].",this)".'" ></td>';
				}
				else{
 					echo '<td style="width:20ex">'.$oneTeacherPart['NamePart'].'</td>';
				}
			echo '<td style="width:4ex">'.$oneTeacherPart['loadpart'].'</td>';

			echo '</tr>';
			$theLoad += $oneTeacherPart['loadpart'];
			$totLoadString = '<tr><td class="totLoad">Total load</td><td><br /></td><td><br /></td><td><br /></td><td class="totLoad">'.$theLoad.'</td></tr>';
		}
		

	}
	echo $totLoadString;
	echo '</table>';
	if ($editOneTeacher){
		echo '<p class="editlink">'."<a href=teacherParts.php#headTeacher".$editID.">done</a></p>";
		}
	echo '</div>';

	echo '<p class="SQLquery">'.$theQuery.'</p>';
	mysqli_close($conn);
?>

</body>
</html>
