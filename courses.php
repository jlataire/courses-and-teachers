<?php	
	require 'teachCourseFunctions.php';
?>

<!DOCTYPE html>
<html>
<head><title>Courses</title>
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
<style>
	p,td
	{
		font-family:Verdana;
		font-size:small;
	}
	th{
		font-family:Verdana;
		font-size:small;
	}
	table{
		border-collapse:collapse;
	}
	tr:nth-child(even){background-color:#ddd;}
	td,th{
		padding-top:0.5ex;
		padding-bottom:0.5ex;
		padding-left:0.5em;
		padding-right:0.5em;
	}
	
	td.sembullet{text-align:center;}
	@media screen{
		a:link,a:visited
		{
			text-decoration:none;
			color:blue;
		}
		a:hover
		{
			text-decoration:underline;
		}
	}
</style>
<!-- <script src="jquery-3.3.1.min.js"></script> -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="teachCourseScripts.js"></script>
<script>

<?php
	echo 'var allAY = '.fetchTableContentJSON($conn,'*','AcademicYear','1','startDate').';';
	echo 'var allNrStudents = '.fetchTableContentJSON($conn,'*','linkCourseAY','1','id').';';
	if ($_GET['edit']==1){echo 'var editable = true;';}else{echo 'var editable = false;';}
?>

allAY = allAY.records;
allNrStudents = allNrStudents.records;
// var allTDnrstud;

$(document).ready(function(){
	$(".ECTS").change(function(){updateTable('Courses','ECTS',$(this).attr("data-id"),{value:$(this).val()})});
	$(".HOC").change(function(){updateTable('Courses','HOC',$(this).attr("data-id"),{value:$(this).val()})});
	$(".WPO").change(function(){updateTable('Courses','WPO',$(this).attr("data-id"),{value:$(this).val()})});
	
	$(".sem1").attr("checked",function(){return ($(this).attr("data-checked") == 1);})
		.change(function(){
			if ($(this)[0].checked){ressem = 1;}else{ressem = 0;}
			updateTable('Courses','sem1',$(this).attr("data-id"),{value:ressem});});
			
	$(".sem2").attr("checked",function(){return ($(this).attr("data-checked") == 1);})
		.change(function(){
			if ($(this)[0].checked){ressem = 1;}else{ressem = 0;}
			updateTable('Courses','sem2',$(this).attr("data-id"),{value:ressem});});
	
	$(".sembullet1").html(function(){
		if ($(this).attr("data-val") == 1){return "<i class='fas fa-chevron-circle-left'></i>";}
		else{return "&nbsp;";}});
	$(".sembullet2").html(function(){
		if ($(this).attr("data-val") == 1){return "<i class='fas fa-chevron-circle-right'></i>";}
		else{return "&nbsp;";}
		});
	
	for (y in allAY){
		d = new Date(allAY[y].startDate);
		$("#headRow").append('<th>' +  d.getFullYear() + '</th>');
		$(".rowCourse").append(function(){
			cid = $(this).attr("data-courseID");
			mytd = $("<td></td>");
			if (editable){
				myinput = $("<input></input>").attr("size",4)
					.change(function(){updateNrStudents($(this).attr("data-cid"),$(this).attr("data-ay"),$(this).val());});
				mytd.append(myinput);
				myc = myinput;
			}
			else{myc = mytd;}
			myc.attr("id","nstud_" + cid + '_' + allAY[y].id)
				.attr("data-cid",cid)
				.attr("data-ay",allAY[y].id);
			return mytd;
		});
	}
	for (n in allNrStudents){
		myc = $("#nstud_" + allNrStudents[n].courseID + '_' + allNrStudents[n].AYID);
		if (editable){myc.val(allNrStudents[n].nrStudents);}
		else{myc.html(allNrStudents[n].nrStudents);}
	}
});
</script>

</head>
<body>
<p id="AJAXresponse">The AJAX response</p>
<?php 

	
	echo navigationBar();
	echo '<h1>All Courses</h1>';
	if ($_GET['edit'] != 1){
		echo '<p><a href="courses.php?edit=1">Edit</a></p>';
	}
//	echo 'Connected';
	switch ($_GET['orderby']){
		case 'ECTS':
			$orderby = "ECTS, Courses.NameCourse";
			break;
		case 'HOC':
			$orderby = "HOC, WPO, Courses.NameCourse";
			break;
		case 'WPO':
			$orderby = "WPO, HOC, Courses.NameCourse";
			break;
		case 'sem1':
			$orderby = "sem1 DESC, Courses.NameCourse";
			break;
		case 'sem2':
			$orderby = "sem2 DESC, Courses.NameCourse";
			break;
		case 'titular': 
			$orderby = "Titular.lastname,Courses.NameCourse";
			break;
		default:
			$orderby = "Courses.NameCourse";
	}


	$allCourses = $conn->query('SELECT Courses.id AS CourseID, Titular.firstname, Titular.lastname, Courses.NameCourse, ECTS, HOC, WPO, sem1, sem2, caliID 
	FROM `Courses` INNER JOIN Teachers AS Titular ON Courses.TitularID=Titular.id WHERE 1 ORDER BY '.$orderby);
	echo '<table>';
	echo '<tr id="headRow">
	<th>CaLi</th>
	<th><a href="courses.php">Title</a></th>
	<th><a href="courses.php?orderby=titular">Titular</a></th>
	<th><a href="courses.php?orderby=ECTS">ECTS</a></th>
	<th><a href="courses.php?orderby=HOC">HOC</a></th>
	<th><a href="courses.php?orderby=WPO">WPO</a></th>
	<th><a href="courses.php?orderby=sem1">sem 1</a></th>
	<th><a href="courses.php?orderby=sem2">sem 2</a></th></tr>';
	while ($oneCourse=mysqli_fetch_array($allCourses)){
		echo '<tr class="rowCourse" data-courseID="'.$oneCourse['CourseID'].'">';
		echo '<td style="font-size:x-small"><a href="https://caliweb.cumulus.vub.ac.be/caliweb/?page=course-offer&id='.$oneCourse['caliID'].'&anchor=1&target=pr&year='.$academicYearShort.'&language=en&output=html">CaLi</a></td>';
		echo '<td><a href="index.php#headCourse'.$oneCourse['CourseID'].'" target="windowCourses">'.$oneCourse['NameCourse'].'</a> 
		</td>
				<td>'.$oneCourse['firstname'].' '.$oneCourse['lastname'].'</td>';
		if ($_GET['edit']==1){
			echo '<td><input class="ECTS" data-id="'.$oneCourse['CourseID'].'" type="text" size="3" value='.$oneCourse['ECTS'].' /></td>';
			echo '<td><input class="HOC" data-id="'.$oneCourse['CourseID'].'" type="text" size="3" value='.$oneCourse['HOC'].' /></td>';
			echo '<td><input class="WPO" data-id="'.$oneCourse['CourseID'].'" type="text" size="3" value='.$oneCourse['WPO'].' /></td>';
			echo '<td><input class="sem1" data-id="'.$oneCourse['CourseID'].'" type="checkbox" data-checked='.$oneCourse['sem1'].' /></td>';
			echo '<td><input class="sem2" data-id="'.$oneCourse['CourseID'].'" type="checkbox" data-checked='.$oneCourse['sem2'].' /></td>';
		} else{
			echo '<td>'.$oneCourse['ECTS'].'</td><td>'.$oneCourse['HOC'].'</td><td>'.$oneCourse['WPO'].'</td>
				<td class="sembullet1" data-val='.$oneCourse['sem1'].'></td><td class="sembullet2" data-val='.$oneCourse['sem2'].'></td>';
		}
		echo '</tr>';
	}
	echo '</table>';

?>

<p>
<form method="post" action="courses.php">
<input type="hidden" name="actionType" value="addCourse" />
	<select name="titularID">
	<?php
	echo teacherOptionList($conn);
	?>
	</select>
	<input name="name" type="text" />
	<input type="submit" value="Submit" />
</form>
</p>

<p>
<form action="fetchCoursesNrStudTSV.php" method="post">
<input type="hidden" name="tableSpec" rows="5" cols="80"
value=" *, `courses`.id AS cid FROM `courses` INNER JOIN teachers ON teachers.id=courses.TitularID WHERE 1 ORDER BY teachers.lastname, courses.NameCourse" />
<input type="submit" value="Download as .tsv">
</form>
</p>
<br />
</body>
</html>


<?php
    mysqli_close($conn);
	?>
