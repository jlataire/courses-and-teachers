# Database program for the distribution of teaching duties

Requires PHP and MYSQL

Generate an empty database with template `DB_struct.sql` with _foreign key checks_ disabled.

Requires a file `openMySQLconn.php` with

```php
	$conn = new mysqli("server","username",'password',"databasename");
```

