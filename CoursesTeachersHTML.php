<?php
	require 'teachCourseFunctions.php';
    if (!empty($_GET['showLoad']) and (strcmp($_GET['showLoad'],'false')==0))
    {
        $showLoad = false;
    }else
    {
        $showLoad = true;
        
    }
    if ($_GET['dl'] == 1)
    {
        $thefilename = 'ELEC_CoursesAndTeachers'.getLastChangeTime($conn,1);
        if ($showLoad){
            $thefilename .= '_WITH_LOAD';
        }
        header('Content-Type: text/html');
        header('Content-Disposition: attachment; filename="'.$thefilename.'"');
        $dl = 1;
    }
    else {$dl = 0;}
    ?>
<!DOCTYPE html>
<html>
<head>
<script>
function goToExport(){
    location.href = location.href+'&dl=1';
}
</script>
<meta charset="UTF-8">
<?php
echo'<title>ELEC - Courses and Teachers '.$academicYear.'</title>';

echo '<style>';
echo file_get_contents('teachCourseStyles.css');
echo '</style>';
 ?>

</head>
<body>

<?php 
	
	date_default_timezone_set("Europe/Brussels");

    if (!$dl){
        echo '<p><a href="javascript:goToExport()">Export page</a></p>';
    }
    
	echo '<p>Last update '.getLastChangeTime($conn).'</p>';
	
	echo '<h1>Academic Year '.$academicYear.'</h1>';
	
	
	switch ($_GET['orderby']){
		case "load":
			$orderby = 1;
			break;
		case "enddate":
			$orderby = 2;
			break;
        case "startdate":
            $orderby = 3;
            break;
        case "type":
            $orderby = 4;
            break;
		default:
			$orderby = 0;
	}
	
// 	if ($showLoad)
// 	{echo 'Showing the load - <a href=?showLoad=false>Hide</a>';}
	//echo navigationBar();

	// Show all the courses

		$allCourses = $conn->query('SELECT Courses.id AS CourseID, Titular.firstname, Titular.lastname, Courses.NameCourse, sem1, sem2 FROM `Courses` INNER JOIN Teachers AS Titular ON Courses.TitularID=Titular.id WHERE 1 ORDER BY Courses.NameCourse');
	echo '<h2>All Courses</h2>';
	echo '<table>';
	echo '<tr><th><br /></th><th>Titular</th><th>Sem 1</th><th>Sem 2</th></tr>';
	while ($oneCourse=mysqli_fetch_array($allCourses)){
		echo '<tr><td><a href="#headCourse'.$oneCourse['CourseID'].'">'.$oneCourse['NameCourse'].'</a> 
		</td>
				<td>'.$oneCourse['firstname'].' '.$oneCourse['lastname'].'</td>';
		if ($oneCourse['sem1']){echo '<td>&bullet;</td>';}else{echo '<td>&nbsp;</td>';}
		if ($oneCourse['sem2']){echo '<td>&bullet;</td>';}else{echo '<td>&nbsp;</td>';}
		echo '</tr>';
	}
	echo '</table>';
	
	// Show all the teachers
	echo '<hr>';
	$allTeachers = getTeachersLoadSQL($conn,2,$orderby);
	echo '<h2>All teachers</h2>';
	echo '<table>';
	if ($showLoad){ echo '<tr><th>Load</th><th></th><th></th><th class="date">Start</th><th class="date">Length PhD*</th><th class="date">End contract</th></tr>';}
	while ($oneTeacher=mysqli_fetch_array($allTeachers)){
		if (!$showLoad and !strcmp($oneTeacher['lastname'],'assigned')){continue;}
		echo '<tr>';
			if ($showLoad){
			if ($oneTeacher['active']){
				echo '<td>';
			}
			else
			{
				echo '<td class="redboldcell">';
			}
			echo $oneTeacher['totalLoad'].'</td>';
		}
		echo '<td class="firstname">'.$oneTeacher['firstname'].'</td><td><a href="#headTeacher'.$oneTeacher['id'].'">'.$oneTeacher['lastname'].'</a></td>';
		if ($showLoad){
            echo '<td>'.$oneTeacher['startDate'].'</td>';
            echo '<td>'.timeUntilStartAcademicYear($oneTeacher['startDate']).'</td>';
			$oneEndDate = $oneTeacher['endDate'];

			$endDateMode = endDateMode($oneEndDate);
			switch ($endDateMode){
				case 3:
					echo'<td class="redboldcell">';break;
				case 2:
					echo'<td class="redcell">';break;
				case 1:
					echo '<td class="orangecell">';break;
				default:
					echo '<td>';
			}
			echo $oneEndDate.'</td>';
			if ($showLoad){
				echo '<td>'.$oneTeacher['comments'].'</td>';
			}
// 			echo '<td>'.date_interval_format(date_diff($oneDateP,$endAcademicYear,false),"%r%a").'</td>';
		}
		echo "</tr>\r";
	}
	echo "</table>\r";
    if ($showLoad){
        echo '<p>*on '.$academicYearStart.'</p>';
    }
	

	
	// Courses and teachers
	

	$theQuery = 'SELECT Courses.Comments,
	 Courses.NameCourse,
	 Courses.id AS CourseID,
	 `Course part`.TeacherID, 
	 Teachers.lastname,
	 Teachers.firstname,
	 Teachers.active,
	 `Course part`.typePart,
	 `Course part`.NamePart,
	 `Course part`.id AS partID, 
	 Titularis.lastname AS lastnameTitular, 
	 Titularis.firstname AS firstnameTitular, 
	 TitularID, 
	 `Course part`.loadpart,
	 `Course part`.responsible, 
	 Courses.caliID, 
	 sem1, sem2,
	 ECTS, HOC, WPO 
	FROM `Course part` RIGHT JOIN Teachers ON `Course part`.TeacherID=Teachers.id 
	RIGHT OUTER JOIN Courses ON `Course part`.CourseID=Courses.id 
	INNER JOIN Teachers AS Titularis ON Courses.TitularID=Titularis.id';
	
	$theQuery .=  ' WHERE 1';

	$theQuery .= ' ORDER BY Courses.NameCourse,`Course part`.typePart,`Course part`.responsible DESC,Teachers.lastname';
	
	//echo $theQuery.'<br />';
	$allCoursePartsSQL = $conn->query($theQuery);
	$currentCourseID = 0;
	$teacherOptionList = teacherOptionList($conn);

	echo '<hr>';
	
	echo '<h2>Courses and Teachers</h2>';

	while ($oneCoursepart=mysqli_fetch_array($allCoursePartsSQL)){

		if ($currentCourseID != $oneCoursepart['CourseID']){
			if ($currentCourseID != 0){
				echo '</table></div>';
				}
			echo '<div class="courseDiv">';

            echo '<header><h3 id="headCourse'.$oneCoursepart['CourseID'].'"> ';

            if (empty($oneCoursepart['caliID'])){
                echo $oneCoursepart['NameCourse'];
            }
            else{
                echo '<a href="https://caliweb.cumulus.vub.ac.be/caliweb/?page=course-offer&id='.$oneCoursepart['caliID'].'&anchor=1&target=pr&year=1819&language=en&output=html">'.$oneCoursepart['NameCourse'].'</a>';
            }
			
			echo ' <span class="coursetitular">- '.$oneCoursepart['firstnameTitular'].' '.$oneCoursepart['lastnameTitular'].'</span>';
			echo '<span class="semCourse" title="semester">';
			if ($oneCoursepart['sem1']){echo '&#10112; ';}
			if ($oneCoursepart['sem2']){echo '&#10113; ';}
			echo '</span>';
			echo '</h3></header>';
			echo '<p class="comment">';
			echo 'ECTS '.$oneCoursepart['ECTS'].' - HOC '.$oneCoursepart['HOC'].' - WPO '.$oneCoursepart['WPO'];
			if (($showLoad) and (!empty($oneCoursepart['Comments']))){
				echo ' '.$oneCoursepart['Comments'];
			}
			echo '</p>';
			echo '<table>';
		
		}
		$currentCourseID = $oneCoursepart['CourseID'];
		if (!empty($oneCoursepart['partID'])){
		if ((!$oneCoursepart['active']) or ($oneCoursepart['TeacherID'] == 1)){ // either not active, or not assigned (Teacher ID = 1)
			echo '<tr class="notActive">';
		}
		else if ($oneCoursepart['responsible']){
				echo '<tr class="responsible">';
			}
			else{
				echo '<tr>';
			}
			echo '<td style="width:27ex">';

			echo '<a href="#headTeacher'.$oneCoursepart['TeacherID'].'">'.$oneCoursepart['firstname'].' '.$oneCoursepart['lastname'].'</a>';
			
			echo '</td>';

			echo '<td style="width:5ex">'.$oneCoursepart['typePart'].'</td><td style="width:40ex">'.$oneCoursepart['NamePart'].'</td>';
			if ($showLoad){
				if ($oneCoursepart['active']){
					echo '<td style="width:4ex;text-align:right">';
				}else{
					echo '<td class="redboldcell">';
				}
				echo $oneCoursepart['loadpart'].'</td>';
			}

			echo '</tr>';
		}
		

	}
	echo "</table></div>\r";





	// Teachers and Courses

	
	echo "\r<hr /><h2>Teachers and Courses</h2>";
	
	$theQuery = 'SELECT Courses.NameCourse,Courses.id AS CourseID,`Course part`.TeacherID, Teachers.comments, Teachers.lastname,Teachers.firstname,Teachers.active,`Course part`.typePart,`Course part`.NamePart,`Course part`.id AS partID, Titularis.lastname AS lastnameTitular, Titularis.firstname AS firstnameTitular, TitularID, `Course part`.loadpart, `Course part`.responsible,sem1, sem2  
	FROM `Course part` RIGHT JOIN Teachers ON `Course part`.TeacherID=Teachers.id RIGHT OUTER JOIN Courses ON `Course part`.CourseID=Courses.id INNER JOIN Teachers AS Titularis ON Courses.TitularID=Titularis.id'; 

	$theQuery .=  ' WHERE 1';

	$theQuery .= ' ORDER BY Teachers.lastname, Teachers.firstname, Courses.NameCourse,`Course part`.typePart';
	
	//echo $theQuery.'<br />';
	$allCoursePartsSQL = $conn->query($theQuery);
	$currentTeacherID = 0;
	$teacherOptionList = teacherOptionList($conn);

	while ($oneTeacherPart=mysqli_fetch_array($allCoursePartsSQL)){

		if ($currentTeacherID != $oneTeacherPart['TeacherID']){
			if ($currentTeacherID != 0){
				if ($showLoad){
					echo $totLoadString;
				}
				echo '</table></div>';
				}
			echo '<div class="courseDiv">';
			$theLoad = 0;

			echo '<header><h3 id="headTeacher'.$oneTeacherPart['TeacherID'].'"';
			if (!$oneTeacherPart['active']){
				echo ' class="notActiveTeacher"';
			}
			echo '>'.$oneTeacherPart['firstname'].' '.$oneTeacherPart['lastname'].'</h3></header>';
			
			if (($showLoad) and (!empty($oneTeacherPart['comments']))){
			echo '<p class="comment">'.$oneTeacherPart['comments'].'</p>';
			}
			
			$queryTitular = 'SELECT Courses.NameCourse,Courses.id AS CourseID FROM Courses INNER JOIN Teachers ON Courses.TitularID=Teachers.id WHERE Teachers.id='.$oneTeacherPart['TeacherID'];
			
// 			echo '<p>'.$queryTitular.'</p>';
			$coursesAsTitular = $conn->query($queryTitular);
			if (mysqli_num_rows($coursesAsTitular) > 0)
			{
// 				echo '<h3>Titular of</h3>';
				echo '<table><tr><th style="text-align:left">Titular of</th><th></th></tr>';
				while ($oneCourseTitular = mysqli_fetch_array($coursesAsTitular)){
					echo '<tr><td style="width:35ex"><a href="#headCourse'.$oneCourseTitular['CourseID'].'">'.$oneCourseTitular['NameCourse'].'</a></td></tr>';
				}
				echo '</table>
					<table>
					<tr><th style="text-align:left">Teaching</th><th></th><th></th><th></th></tr>';
			}
			else
			{ echo '<table>';}

		}
		$currentTeacherID = $oneTeacherPart['TeacherID'];
		
		
		
		if (!empty($oneTeacherPart['partID'])){
			if ($oneTeacherPart['responsible']){
				echo '<tr class="responsible">';
			}
			else{
				echo '<tr>';
			}
			echo '<td style="width:40ex">';
			echo '<a href="#headCourse'.$oneTeacherPart['CourseID'].'">'.$oneTeacherPart['NameCourse'].'</a> ';

			echo '</td>';
			
			echo '<td style="width:4ex" title="semester">';
			if ($oneTeacherPart['sem1']){echo '&#10112; ';}else{echo '&nbsp;&nbsp;&nbsp;';}
			if ($oneTeacherPart['sem2']){echo '&#10113; ';}
			echo '<td style="width:5ex">'.$oneTeacherPart['typePart'].'</td>';
			echo '<td style="width:20ex">'.$oneTeacherPart['NamePart'].'</td>';
			if ($showLoad){		
				echo '<td>'.$oneTeacherPart['loadpart'].'</td>';
				$theLoad += $oneTeacherPart['loadpart'];
				$totLoadString = '<tr><td class="totLoad">Total load</td><td><br /></td><td><br /></td><td><br /></td><td class="totLoad">'.$theLoad.'</td></tr>';
			}
			echo "</tr>\r";
		}
		

	}
	if ($showLoad){
		echo $totLoadString;
	}
	echo "</table></div>\r";
	
	mysqli_close($conn);
?>

</body>
</html>
