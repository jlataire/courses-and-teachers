<?php
// header("Access-Control-Allow-Origin: *");
header("Content-Type: text/tsv; charset=UTF-8");
header('Content-Disposition: attachment; filename="TableContents.tsv"');

require 'openMySQLconn.php';
if ($conn -> connect_error){die("Could not connect");}

if (isset($_POST['tableSpec']) and !empty($_POST['tableSpec'])){
	$tableSpec = $_POST['tableSpec'];
	$tableSpec = $conn->real_escape_string($tableSpec);
	
	
	
	$selectQuery = "SELECT $tableSpec";
	
	$result = $conn->query($selectQuery);
	
	if ($conn->errno){
		die($conn->error.' query: '.$selectQuery);
	}

	$outp = "";
	$sep = "\t";
	$firstRow = true;
	while($rs = $result->fetch_array(MYSQLI_ASSOC)) {
		if ($firstRow){
			foreach ($rs as $fieldname => $fieldcontent){
				$outp .= '"'.$fieldname.'"'.$sep;
			}
			$outp = rtrim($outp,$sep);
			$outp .= "\r\n";
			$firstRow = false;
		}
			
		foreach ($rs as $fieldname => $fieldcontent){
			if ($fieldname == "NameCourse"){
				$outp .= '=HYPERLINK("https://caliweb.cumulus.vub.ac.be/caliweb/?page=course-offer&id='.$rs['caliID'].'&anchor=1&target=pr&year=1819&language=en&output=html","'.$fieldcontent.'")'.$sep;
// 				$outp .= '"'.$fieldcontent.'"'.$sep;
			}else{
				$outp .= '"'.$fieldcontent.'"'.$sep;
				}
		}

		
		$outp = rtrim($outp,$sep);
		$outp .= "\r\n";
	}
} else
{
	$outp = "No table name provided";
}

$conn->close();

echo($outp);
?>