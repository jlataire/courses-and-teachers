<!DOCTYPE html>
<html>
<head><title>Teachers</title>
<script src="teachCourseScripts.js"></script>
<link rel="stylesheet" type="text/css" href="teachCourseStyles.css">
<style>

	td
	{
		font-size:small;
		padding-left:1ex;
		padding-right:1ex;
		padding-top:2pt;
		padding-bottom:2pt;
		border-top:thin #ccc solid;
	}
	td.notActive
	{
		color:red;
		font-weight:bold;
	}

	td.redcell{
		color:red;
	}
	td.orangecell{
		color:#fa0;
	}
	td.redboldcell{
		color:red;
		font-weight:bold;
	}
	tr.odd{
		background-color:#eee;
/* 		background-color:none; */
	}
</style>
<!-- <script src="jquery-3.3.1.min.js"></script> -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script>
$(document).ready(function(){
	setAllTeacherSelectors();
});
</script>
</head>

<body>

<p id="AJAXresponse">The AJAX response</p>
<p><a href="makeBU.php">Back up database</a></p>

<?php 


	
	require 'teachCourseFunctions.php';
	if (empty($_GET['onlyactive']) or (!strcmp($_GET['onlyactive'],'true'))){
		$onlyactive = 1;
	}
	else{
		$onlyactive = 0;
	}
	switch ($_GET['orderby']){
		case "load":
			$orderby = 1;
			break;
		case "enddate":
			$orderby = 2;
			break;
        case "startdate":
            $orderby = 3;
            break;
        case "type":
            $orderby = 4;
            break;
		default:
			$orderby = 0;
	}
	echo '<h1>Teachers</h1>';
	
	echo navigationBar();
// 	echo sprintf("%u-11-01",$academicYearNum);
	
	if (!empty($_GET['modifID']) and is_numeric($_GET['modifID'])){
		$modifID = $_GET['modifID'];
		$theQuery = 'SELECT * FROM teachers WHERE id='.$modifID;
		$teacherModifSQL = $conn->query($theQuery);
		$teacherM = mysqli_fetch_array($teacherModifSQL);
        $teacherTypeList = teacherTypeList($conn);
        
        echo '<p><select class="teachSelect" data-selID="'.$teacherM['typeID'].'" id="selectTypeTeacher'.$modifID.'" onchange="updateTeacherType('.$modifID.')">';
        echo $teacherTypeList;
        echo '</select></td>';
//         echo '<script>setSelected("selectTypeTeacher'.$modifID.'","'.$teacherM['typeID'].'")</script>';
        
		echo '<input type="text" name="firstname" placeholder="First name" value="'.$teacherM['firstname'].'"
		onchange="updateTable('."'teachers','firstname',".$modifID.',this)" />
		<input type="text" name="firstname" placeholder="Last name" value="'.$teacherM['lastname'].'"
		onchange="updateTable('."'teachers','lastname',".$modifID.',this)" />
        <input type="text" name="startDate" placeholder="Start contract yyyy-mm-dd" value="'.$teacherM['startDate'].'"
        onchange="updateTable('."'teachers','startDate',".$modifID.',this)" />
		<input type="text" name="endDate" placeholder="End contract yyyy-mm-dd" value="'.$teacherM['endDate'].'"
		onchange="updateTable('."'teachers','endDate',".$modifID.',this)" />
		<input type="text" name="comments" placeholder="Comments" value="'.$teacherM['comments'].'"
		onchange="updateTable('."'teachers','comments',".$modifID.',this)" size="50" />
		</p>';
		
		echo '<p><a href="teachers.php">done</a></p>';
	}

	else{
		$odd = true;
		$allTeachers = getTeachersLoadSQL($conn,0,$orderby);
		echo '<table>';
		echo '<tr><th></th><th><a href="?orderby=type">Type</a></th><th colspan="2"><a href="?orderby=name">Name</a></th><th><a href="?orderby=load">Load</a></th><th><a href="?orderby=startdate">Start date</a></th><th>Length</th><th><a href="?orderby=enddate">End contract</a></th></tr>';
		while ($oneTeacher=mysqli_fetch_array($allTeachers)){
			if ((($onlyactive and $oneTeacher['active']) or ($oneTeacher['totalLoad'] > 0)) or (!$onlyactive)){
				if ($oneTeacher['active']){$active = 'checked ';}else{$active = '';}
				if ($odd){
					echo '<tr class="odd">';
				}else{
					echo "<tr>";
				}
				echo '<td><input type="checkbox" '.$active.'onchange="updateActiveTeacher('.$oneTeacher['id'].',this)"></td>
                    <td>'.$oneTeacher['teacherType'].'</td>
					<td>'.$oneTeacher['firstname'].'</td><td>
					<a href="teacherParts.php#headTeacher'.$oneTeacher['id'].'" target="windowTeachers">'.$oneTeacher['lastname'].'</a></td>';
				if (!$oneTeacher['active']){
					echo '<td class="notActive">';
					}
				else{
					echo '<td>';
				}
				echo $oneTeacher['totalLoad'].'</td>';
                echo '<td>'.$oneTeacher['startDate'].'</td>';
                echo '<td>'.timeUntilStartAcademicYear($oneTeacher['startDate']).'</td>';
				if (false){// when true, makes end dates immediately editable
					echo '<td><input type="text" name="endDate" value="'.$oneTeacher['endDate'].'"
						onchange="updateTable('."'teachers','endDate',".$oneTeacher['id'].',this)" /></td>';
				}else
				{
					$endDateMode = endDateMode($oneTeacher['endDate']);
					switch ($endDateMode){
						case 3:
							echo'<td class="redboldcell">';break;
						case 2:
							echo'<td class="redcell">';break;
						case 1:
							echo '<td class="orangecell">';break;
						default:
							echo '<td>';
					}
					echo $oneTeacher['endDate'].'</td>';
					echo '<td><a href="?modifID='.$oneTeacher['id'].'">edit</a></td>';
					echo '<td>'.$oneTeacher['comments'].'</td>';
				}
				echo "</tr>\r";
				$odd = !$odd;
			}
		}
		echo '</table>';
		echo '<form method="post" action="teachers.php">
				<input type="hidden" name="actionType" value="addTeacher" />
				<input name="firstname" placeholder="First name" type="text" /><input name="lastname" placeholder="Last name" type="text">
				<input type="submit" value="add" />
			</form>';

	}

?>

</body>
</html>

<?php
	mysqli_close($conn);
	?>
